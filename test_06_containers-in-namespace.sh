#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/setup.sh

printf "%s\n" "-- Running shm test in orderly"
podman run $PARAMS -d --name orderly $IMAGE ./tst_shm > /dev/null

printf "%s\n" "-- Running shm test in confusion"
podman run $PARAMS --name confusion --ipc container:orderly $IMAGE ./tst_shm > /dev/null

cleanup

