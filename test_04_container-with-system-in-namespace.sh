#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/setup.sh

printf "%s\n" "-- Running shm test in confusion"
podman run $PARAMS -d --name confusion --ipc host $IMAGE ./tst_shm > /dev/null

printf "%s\n" "-- Running shm"
./tst_sys_shm >/dev/null

cleanup

